<?php
    $faculty_list = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

    $student_list = [
        [
            "id" => 1,
            "fullName" => "Nguyễn Văn A",
            "faculty" => "Khoa học máy tính"
        ],
        [
            "id" => 2,
            "fullName" => "Trần Thị B",
            "faculty" => "Khoa học máy tính"
        ],
        [
            "id" => 3,
            "fullName" => "Nguyễn Hoàng C",
            "faculty" => "Khoa học vật liệu"
        ],
        [
            "id" => 4,
            "fullName" => "Đinh Quang D",
            "faculty" => "Khoa học vật liệu"
        ]
    ];


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/students.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <form action="#" class="query-form">
            <div class="faculty-box input-box">
                <label for="">Khoa</label>
                <select id="faculty" class="select-field">
                    <?php foreach($faculty_list as $key => $value) { ?>
                        <option value=<?php echo $key ?>><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="keyword-box input-box">
                <label for="">Từ khóa</label>
                <input id="keyword" class="input-field" type="text" value="">
            </div>
            <div class="btn-box input-box">
                <div id="delete-btn" class="delete-btn">Xóa</div>
                <button id="find-btn" class="button-submit" type="submit">Tìm kiếm</button>
            </div>
        </form>
        <div class="students-num-box">
            <span>Số sinh viên tìm thấy: XXX</span>
            <form action="register.php">
                <button class="button-add">Thêm</button>
            </form>
        </div>
        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th style="width:40%">Khoa</th>
                <th>Action</th>
            </tr>
            <?php
                foreach($student_list as $student) {
            ?>
            <tr>
                <td><?php echo $student['id'] ?></td>
                <td><?php echo $student['fullName'] ?></td>
                <td><?php echo $student['faculty'] ?></td>
                <td>
                    <button class="button-delete">Xóa</button>
                    <button class="button-modify">Sửa</button>
                </td>
            </tr>
            <?php
                }
            ?>
        </table>
    </div>
</body>
<script>
    var deleteBtn = document.getElementById("delete-btn");
    var findBtn = document.getElementById("find-btn");
    var faculty = document.getElementById("faculty");
    var keyword = document.getElementById("keyword");
    
    deleteBtn.onclick = () => {
        faculty.value = "";
        keyword.value = "";
        sessionStorage.clear();
    };

    findBtn.onclick = () => {
        sessionStorage.setItem("faculty", faculty.value);
        sessionStorage.setItem("keyword", keyword.value);
    };

    if (sessionStorage.getItem("faculty") != null) {
        faculty.value = sessionStorage.getItem("faculty");
    }
    if (sessionStorage.getItem("keyword") != null) {
        keyword.value = sessionStorage.getItem("keyword");
    }
</script>
</html>