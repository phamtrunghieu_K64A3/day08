<?php
    session_start();

    $gender_list = ["Nam", "Nữ"];
    $faculty_list = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

    /* Datepicker của bootstrap đã tự động loại bỏ chữ cái cũng như chuẩn hóa date đầu vào
        nên hàm này sẽ ko bao giờ được thực thi.
    */
    function isValidDate($date, $format = 'd/m/Y'){
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }
    
    $errors = "";

    if (isset($_POST["submit"])) {
        if (empty($_POST["userName"]))
            $errors = $errors . '<div class="errors">Hãy nhập tên.<br></div>';
        if (empty($_POST["gender"]))
            $errors = $errors . '<div class="errors">Hãy chọn giới tính.<br></div>';
        if (empty($_POST["faculty"]))
            $errors = $errors . '<div class="errors">Hãy chọn phân khoa.<br></div>';
        if (empty($_POST["birthday"]))
            $errors = $errors . '<div class="errors">Hãy nhập ngày sinh.<br></div>';
        elseif (!isValidDate($_POST["birthday"]))
            $errors = $errors . '<div class="errors">Hãy nhập ngày sinh đúng định dạng.<br></div>';
        if ($_FILES["img"]["error"] == 0) {
            $fileExt = explode('.', $_FILES['img']['name']);
            $fileFormat = strtolower(end($fileExt));
            $allowed = array('jpg', 'jpeg', 'png', 'pdf');
            if (!in_array($fileFormat, $allowed)) {
                $errors = $errors . '<div class="errors">Hãy chọn file có định dạng jpg, jpeg, png hoặc pdf.<br></div>';
            }
            else {
                if (!file_exists('upload')) {
                    mkdir('upload', 0777, true);
                }

                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $date_time = date('YmdHis', time());

                $fileNameNew = implode(".", array_slice($fileExt, 0, -1)) . '_' . $date_time . '.' . $fileFormat;
                $path = 'upload/' . $fileNameNew;
                move_uploaded_file($_FILES['img']['tmp_name'], $path);
                $_POST['img'] = $path;
            }
        }

        if (empty($errors)) {
            $_SESSION = $_POST;
            header('Location: '.'confirm.php');            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <?php
            if(!empty($errors)) {
                echo $errors . "<br>";
            }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="input-box name-box">
                <label for="" class="label-input">
                    Họ và tên
                    <span class="not-empty">*</span>
                </label>
                <input type="text" class="text-field" name="userName">
            </div>
            <div class="input-box gender-box">
                <label for="" class="label-input">
                    Giới tính
                    <span class="not-empty">*</span>
                </label>
                <?php for ($i = 0; $i < count($gender_list); $i++) { ?>
                    <input type="radio" name="gender" class="radio-field" value=<?php echo $i+1; ?>>
                    <label for=""><?php echo $gender_list[$i]; ?></label>
                <?php } ?>
            </div>
            <div class="input-box faculty-box">
                <label for="faculty" class="label-input">
                    Phân khoa
                    <span class="not-empty">*</span>
                </label>
                <select name="faculty" id="faculty" class="select-field">
                    <?php foreach($faculty_list as $key => $value) { ?>
                        <option value=<?php echo $key ?>><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="input-box birthday-box">
                <label for="" class="label-input">
                    Ngày sinh
                    <span class="not-empty">*</span>
                </label>        
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" placeholder="dd/mm/yyyy" name="birthday"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="input-box address-box">
                <label for="" class="label-input">
                    Địa chỉ
                </label>
                <input type="text" class="text-field" name="address">
            </div>
            
            <div class="input-box image-box">
                <label for="" class="label-input">
                    Hình ảnh
                </label>
                <input type="file" id="img" name="img">
            </div>

            <div class="button-box">
                <button class="button-submit" type="submit" name="submit">Đăng ký</button>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datetimepicker({
                format:'DD/MM/YYYY',
            })
        });
    </script>
</body>
</html>


